class Project():
	
	name = ""
	user = ""
	host = ""
	remote_dir = "/"
	mount_point = ""
	
	def set_name(self, name):
		self.name = name

	def get_name(self):
		return self.name

	def set_user(self, user):
		self.user = user

	def get_user(self):
		return self.user

	def set_host(self, host):
		self.host = host

	def get_host(self):
		return self.host

	def set_remote_dir(self, remote_dir):
		self.remote_dir = remote_dir

	def get_remote_dir(self):
		return self.remote_dir

	def set_mount_point(self, mount_point):
		self.mount_point = mount_point

	def get_mount_point(self):
		return self.mount_point