import sublime 
import sublime_plugin

from project_manager import ProjectManager
from project_mounter import ProjectMounter

class MountCommand(sublime_plugin.WindowCommand):

    # Constants
    CONST_PLUGGIN_NAME = 'Project Mount'
    CONST_MOUNT = 'mount'
    CONST_UNMOUNT = 'unmount'
    CONST_UNMOUNT_ALL = 'unmount_all'

    settings = ''
    project_manager = False

    mounted_project_list = []
    unmounted_project_list = []
    
    def run(self, action):
        if (sublime.platform() != "linux"):
            sublime.error_message("This plugin supports Linux only.")
            return False

        self.project_manager = ProjectManager()
        self.load_plugin_settings()
        if (action == self.CONST_UNMOUNT_ALL):
            self.unmount_all()
            return

        self.show_project_input(action)

    def show_project_input(self, action):
        if (action == self.CONST_MOUNT):
            self.window.show_quick_panel(self.unmounted_project_list, self.mount)
        elif (action == self.CONST_UNMOUNT):
            self.window.show_quick_panel(self.mounted_project_list, self.unmount)
        else:
            return False

    def load_plugin_settings(self):
        self.build_menu_lists()

    def build_menu_lists(self):
        self.reset_project_lists()      
        self.add_to_list(self.unmounted_project_list, self.get_unmounted_projects())            
        self.add_to_list(self.mounted_project_list, self.get_mounted_projects())

    def reset_project_lists(self):
        self.mounted_project_list = []
        self.unmounted_project_list = []

    def get_mounted_projects(self):
        return self.get_projects_based_on_status(self.CONST_MOUNT)
    
    def get_unmounted_projects(self):
        return self.get_projects_based_on_status(self.CONST_UNMOUNT)
    
    def get_projects_based_on_status(self, status):
        index = 0
        
        projects = []
        for project in self.project_manager.get_all_projects():
            if (ProjectMounter.is_project_mounted(project) == status):
                projects.append([])
                projects[index] = project
                index = index + 1

        return projects

    def add_to_list(self, list, projects):
        index = 0
        for project in projects:
            list.append([])
            list[index].append(project.get_name())
            list[index].append(project.get_host())
            index = index + 1

    def mount(self, project_selected):      
        
        if (self.has_escaped_menu(project_selected)):
            return

        project_selected = self.unmounted_project_list[project_selected]
        project = self.project_manager.get_project(project_selected[0])

        if (project):
            project_mounter = ProjectMounter(project)
            result = project_mounter.mount()

            self.show_result(result, self.CONST_MOUNT, project)
            return True

        sublime.error_message("Please check that your settings for " + project_selected + " exist and are correct")
        return

    def unmount(self, project_selected):
        
        if (self.has_escaped_menu(project_selected)):
            return

        project_selected = self.mounted_project_list[project_selected]
        project = self.project_manager.get_project(project_selected[0])

        if (project):
            project_mounter = ProjectMounter(project)
            result = project_mounter.unmount()
            self.show_result(result, self.CONST_UNMOUNT, project)
            return True

        sublime.error_message("Please check that your settings for " + project.get_name() + " exist and are correct")
        return

    def unmount_all(self):
        index = 0
        for project in self.mounted_project_list:
            self.unmount(index)
            index = index + 1

    def has_escaped_menu(self, project_selected):
        project_id = int(project_selected)
        # -1 is returned if the user excapes the quick panel so just return false
        if (project_id == -1):
            return True

        return False

    def show_result(self, result, action, project):
        output = ""

        for line in result:
            output = output + line

        if (output == None or output == ""):
            if (action == self.CONST_MOUNT):
                output_end = "mounted "
            else:
                output_end = "unmounted "

            output = "You have successfully " + output_end + project.get_name() + " (" + project.get_host() + ")"

        sublime.message_dialog(output)


class SettingsCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        project_manager = ProjectManager()
        self.view.window().open_file(project_manager.get_settings_file())