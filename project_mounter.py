import subprocess
import sys
import os
import re
from project import Project

class ProjectMounter():
    """docstring for ClassName"""

    project = False

    def __init__(self, project):
        self.set_project(project)

    def set_project(self, project):
        self.project = project

    def get_project(self):
        return self.project


    def mount(self):
        if (self.get_project()):
            result = self.run_command(self.get_mount_command())
            return result

        # sublime.error_message("Please check that your settings for " + project_selected + " exist and are correct")
        return False

    def get_mount_command(self):
        if (self.get_project()):

            host = self.get_project().get_host()
            
            if (self.is_ip_address(host)):
                host = self.enclose_ip_address(host)

            return "sshfs %s@%s:'%s' '%s'" % (self.get_project().get_user(), 
                                            host, 
                                            self.get_project().get_remote_dir(),
                                            self.get_project().get_mount_point())
    
        # sublime.error_message("Please check that your settings for " + self.project.get_name() + " exist and are correct")

    def unmount(self):

        if (self.get_project()):
            result = self.run_command(self.get_unmount_command())
            return result

        return False

    def get_unmount_command(self):
        if (self.get_project()):
            return "fusermount -u '%s'" % self.get_project().get_mount_point()
        
        return False

    def is_ip_address(self, host):
        ip_pattern = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
        if (re.match(ip_pattern, host)):
            return True        
        else:
            return False

    def enclose_ip_address(self, ip):
        return "[%s]" % ip

    def run_command(self, command):
        if (command != False):
            process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
            return iter(process.stdout.readline, b'')

    @staticmethod
    def is_project_mounted(project):
        if (os.path.ismount(project.get_mount_point())):
            return "mount"
        else:
            return "unmount"