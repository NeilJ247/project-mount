import sublime
import sublime_plugin
import json

from project import Project

class ProjectManager():
   
    settings = ""

    CONST_PLUGIN_PATH = sublime.packages_path() + "/Project Mount/"
    CONST_SETTINGS_FILENAME = "ProjectMount.sublime-settings"

    def __init__(self):
        self.settings = json.loads(open(self.get_settings_file()).read())

    def get_project(self, project_name):
        if (self.does_project_exist(project_name) == False or 
            self.verify_settings(project_name) == False):
            return False

        project_settings = self.settings.get(project_name)
        project = Project()
        project.set_name(project_name)
        project.set_user(project_settings.get("user"))
        project.set_host(project_settings.get("host"))
        project.set_remote_dir(project_settings.get("remote_dir"))
        project.set_mount_point(project_settings.get("mount_point"))

        return project

    def get_all_projects(self):
        index = 0
        projects = []
        for project_name in self.settings.keys():
            project = self.get_project(project_name)
            projects.append([])
            projects[index] = project
            index = index + 1

        return projects

    def get_settings_file(self):
        return self.CONST_PLUGIN_PATH + self.CONST_SETTINGS_FILENAME

    def get_project_setting(self, project, setting):
        return self.settings.get(project).get(setting)

    def get_project_settings(self):
        return self.settings

    def setting_is_none(self, settings):
        if (isinstance(settings, list)):
            for setting in settings:
                if (setting == None or setting == ""):
                    return True

        return False

    def does_project_exist(self, project):
        if (project in self.settings):
            return True
        
        return False

    def verify_settings(self, project_name):
        settings = [self.get_project_setting(project_name, "user"), 
                    self.get_project_setting(project_name, "host"),
                    self.get_project_setting(project_name, "remote_dir"), 
                    self.get_project_setting(project_name, "mount_point")]

        if (self.setting_is_none(settings)):
            return False

        return True